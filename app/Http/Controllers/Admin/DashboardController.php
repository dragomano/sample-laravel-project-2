<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class DashboardController extends AdminController
{
    /**
     * Отображение главной страницы админки
     *
     * @return Response
     */
    public function index()
    {
        // Переадресация обычных пользователей при попытке зайти в админку
        if (Auth::check()) {
            $role = Auth::user()->role_id;

            if ($role === 2) {
                return redirect('/');
            }
        }

        $meta = [
            'title' => __('messages.main')
        ];

        return view('admin.index', compact('meta'));
    }
}
