<?php

namespace App\Http\Controllers\Admin;

use App\Models\Event;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class EventController extends AdminController
{
    /**
     * Отображение страницы «Админка - Мероприятия»
     *
     * @return Factory|View
     */
    public function index()
    {
        $events = Event::orderBy('title', 'asc')->get();

        $meta = [
            'title' => __('messages.events')
        ];

        return view('admin.events.index', compact('events', 'meta'));
    }
}
