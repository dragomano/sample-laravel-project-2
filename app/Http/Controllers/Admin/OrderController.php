<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use App\Models\{Order, Event, Role};
use Illuminate\View\View;

class OrderController extends AdminController
{
    /**
     * Отображение страницы «Админка - Заявки»
     *
     * @param Order $orderModel
     *
     * @return Factory|View
     */
    public function index(Order $orderModel)
    {
        // Узнаем идентификатор роли пользователя
        $role_id = Auth::user()->role_id;

        // Для админа показываем все заявки без исключений
        if ($role_id === 1) {
            $events = Event::all();
        } else {
            // По идентификатору роли получаем все мероприятия, закрепленные за данной ролью пользователя
            $events = Role::find($role_id)->events;
        }

        // Получаем идентификаторы всех мероприятий, за которые отвечает текущий пользователь
        $events = array_column($events->toArray(), 'id');

        // Находим все заявки на указанные мероприятия
        $orders = $orderModel->getSortedOrders($events);

        $meta = [
            'title' => __('messages.orders')
        ];

        return view('admin.orders.index', compact('meta', 'orders'));
    }

    /**
     * Удаление заявки с указанным идентификатором
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        if (Auth::user()->role_id === 1)
            Order::destroy($id);

        return redirect()->route('admin.orders');
    }
}
