<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\{Page, Upload};
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class PageController extends AdminController
{
    /**
     * Отображение главной страницы «Админка - Страницы»
     *
     * @return Factory|RedirectResponse|View
     */
    public function index()
    {
        if (Auth::check()) {
            $role = Auth::user()->role_id;

            if ($role !== 1) {
                return redirect()->route('admin');
            }
        }

        $pages = Page::all();

        $meta = [
            'title' => __('messages.pages')
        ];

        return view('admin.pages.index', compact('pages','meta'));
    }

    /**
     * Отображение формы для редактирования заголовка и текста страницы с указанным идентификатором
     *
     * @param int $id
     *
     * @return Factory|View
     */
    public function edit($id)
    {
        $page = Page::find($id);

        $meta = [
            'title' => $page->title
        ];

        return view('admin.pages.edit', compact('meta', 'page'));
    }

    /**
     * Обновление страницы с указанным идентификатором
     *
     * @param Request $request
     * @param         $id
     *
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'       => 'required|max:255',
            //'alias'       => 'required|max:30',
            'description' => 'required|max:255',
            'body'        => 'required',
            'image'       => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        // Обновим соответствующую запись в таблице
        Page::whereId($id)->update([
            'title'       => $request->get('title'),
            'description' => $request->get('description'),
            'body'        => $request->get('body')
        ]);

        $image = $request->file('image');

        // Удалим физически текущую картинку, если добавляем новую
        if ($image !== null) {
            $newFilename = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $image->move($destinationPath, $newFilename);

            $page = Page::find($id)->get();
            if (!empty($page[0])) {
                if ($page[0]->file)
                    $current_image = $page[0]->file->hash;

                if (!empty($current_image))
                    unlink(public_path('/uploads/') . $current_image);
            }

            // Если картинка есть, обновим её, иначе — создадим новую запись в таблице
            Upload::updateOrCreate(
                [
                    'page_id'   => $id
                ],
                [
                    'filename'  => $image->getClientOriginalName(),
                    'hash'      => $newFilename,
                    'mime_type' => $image->getClientMimeType()
                ]
            );
        }

        return redirect()->route('admin.pages.index');
    }
}
