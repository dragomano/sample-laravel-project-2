<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;
use Illuminate\View\View;

class RoleController extends AdminController
{
    /**
     * Отображение страницы «Админка - Роли»
     *
     * @return Factory|RedirectResponse|View
     */
    public function index()
    {
        if (Auth::check()) {
            $role = Auth::user()->role_id;

            if ($role !== 1) {
                return redirect()->route('admin');
            }
        }

        $roles = Role::first()->get();

        $meta = [
            'title' => __('messages.roles')
        ];

        return view('admin.roles.index', compact('roles', 'meta'));
    }
}
