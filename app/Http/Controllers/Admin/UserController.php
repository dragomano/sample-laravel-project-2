<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class UserController extends AdminController
{
    /**
     * Отображение страницы «Админка - Пользователи»
     *
     * @return Factory|View
     */
    public function index()
    {
        $users = User::latest('name')->get();

        $meta = [
            'title' => __('messages.users')
        ];

        return view('admin.users.index', compact('users', 'meta'));
    }
}
