<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\View\View;

class EventController extends Controller
{
    /**
     * Отображение страницы с формой оформления заявки на мероприятие
     *
     * @param $id
     *
     * @return View
     */
    public function show($id)
    {
        $event = Event::find($id);

        $meta = [
            'title' => $event->title,
            'desc'  => $event->description
        ];

        return view('site._order_form', compact('event','meta'));
    }
}
