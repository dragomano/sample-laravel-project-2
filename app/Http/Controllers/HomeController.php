<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Отображение главной страницы сайта
     *
     * @return Renderable
     */
    public function index()
    {
        $page = Page::whereAlias('/')->get()[0];

        $meta = [
            'title' => $page->title
        ];

        return view('site.home', compact('page','meta'));
    }
}
