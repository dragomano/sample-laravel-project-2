<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Jobs\CreateOrderJob;
use App\Mail\SendMail;
use App\Models\Event;

class OrderController extends Controller
{
    /**
     * Сохранение данных заявки в базе данных
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $data = [
            'event_id' => $request->get('event_id'),
            'name'     => $request->get('name'),
            'surname'  => $request->get('surname'),
            'phone'    => $request->get('phone'),
            'email'    => $request->get('email'),
            'grade'    => __('messages.grade_set')[$request->get('grade')],
            'ip'       => $request->getClientIp(),
            'utm'      => str_replace(config('app.url'), '', $_SERVER['HTTP_REFERER'])
        ];

        // Создадим новую задачу, с задержкой выполнения в 1 минуту
        CreateOrderJob::dispatch($data, __('messages.new_order_msg'))->delay(now()->addMinute(1));

        // Отправляем письмо автору заявки
        Mail::to($data['email'])->send(new SendMail($data, __('messages.thanks_for_reg'), 'email.thanks_mail'));

        // Уведомим всех представителей мероприятия
        $event = Event::find($data['event_id']);

        $event_makers = $event->users()->get();
        $emails = [];
        foreach ($event_makers as $maker) {
            $emails[] = $maker->email;
        }

        $data['event_title'] = $event->title;
        Mail::to($emails)->send(new SendMail($data, __('messages.new_order'), 'email.new_order'));

        return redirect()->route('event.show', ['id' => $data['event_id']]);
    }
}
