<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Order;

class CreateOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Указываем количество попыток выполнения задачи (неудавшиеся задачи будут помещены в таблицу failed_jobs)
     *
     * @var int
     */
    public $tries = 1;

    /**
     * Данные для добавления в базу
     *
     * @var
     */
    protected $data;

    /**
     * Текст сообщения при запуске задачи
     *
     * @var
     */
    protected $message;

    /**
     * SendMessage constructor.
     *
     * @param $message
     */
    public function __construct($data, $message)
    {
        $this->data    = $data;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Order::create([
            'event_id' => $this->data['event_id'],
            'name'     => $this->data['name'],
            'surname'  => $this->data['surname'],
            'phone'    => $this->data['phone'],
            'email'    => $this->data['email'],
            'grade'    => $this->data['grade'],
            'ip'       => $this->data['ip'],
            'utm'      => $this->data['utm']
        ]);

        info($this->message);
    }
}
