<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;
    public $subject;
    public $view;

    /**
     * SendMail constructor.
     *
     * @param $data
     * @param $subject
     * @param $view
     */
    public function __construct($data, $subject, $view)
    {
        $this->data    = $data;
        $this->subject = $subject;
        $this->view    = $view;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->view)->with(['data' => $this->data])->subject($this->subject);
    }
}
