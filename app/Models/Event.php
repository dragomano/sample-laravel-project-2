<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Event extends Model
{
    /**
     * Может существовать множество заявок на участие в данном мероприятии
     * Event::find(id)->orders()->get()
     *
     * @return HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * Роль пользователей, отвечающих за мероприятие, только одна
     * Event::find(id)->role
     *
     * @return BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Пользователей, отвечающих за мероприятие, может быть несколько (если у них роли одинаковые)
     * Event::find(id)->users()->get()
     *
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, 'role_id', 'role_id');
    }
}
