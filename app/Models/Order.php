<?php

namespace App\Models;

use Illuminate\Contracts\Translation\Translator;
use Illuminate\Database\Eloquent\Model;
use App\Libraries\DateFormat;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    protected $fillable = ['event_id', 'name', 'surname', 'phone', 'email', 'grade', 'ip', 'utm'];
    protected $perPage  = 2; // Сколько заявок выводить на 1 странице

    /**
     * Преобразуем дату создания в соответствии с правилами русского языка
     *
     * @param $attr
     *
     * @return array|Translator|string|null
     */
    public function getCreatedAtAttribute($attr)
    {
        return DateFormat::post($attr);
    }

    /**
     * Преобразуем дату обновления в соответствии с правилами русского языка
     *
     * @param $attr
     *
     * @return array|Translator|string|null
     */
    public function getUpdatedAtAttribute($attr)
    {
        return DateFormat::post($attr);
    }

    /**
     * Заявка может относиться только к одному мероприятию
     * Order::find(id)->event
     *
     * @return BelongsTo
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * Вывод всех заявок, отсортированных по последней дате, с пагинацией или без
     *
     * @param      $object
     * @param bool $pagination
     *
     * @return mixed
     */
    public function getSortedOrders($object, $pagination = true)
    {
        $query = $this->whereIn('event_id', $object)->latest('created_at');

        if ($pagination) {
            if (\Agent::isMobile()) {
                $orders = $query->simplePaginate();
            } else {
                $orders = $query->paginate();
            }
        } else {
            $orders = $query->get();
        }

        return $orders;
    }
}
