<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Page extends Model
{
    /**
     * К каждой странице может быть приложен 1 файл
     * Page::find(id)->file
     *
     * @return HasOne
     */
    public function file()
    {
        return $this->hasOne(Upload::class);
    }
}
