<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Role extends Model
{
    public $timestamps = false; // Столбцы "дата создания" и "дата обновления" таблице roles нам не нужны

    /**
     * Пользователь с одной и той же ролью может отвечать за множество мероприятий
     * Role::find(id)->events()->get()
     *
     * @return HasMany
     */
    public function events()
    {
        return $this->hasMany(Event::class);
    }

    /**
     * С одной и той же ролью может быть множество пользователей
     * Role::find(id)->users()->get()
     *
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Все заявки, которые может рассматривать обладатель данной роли
     * Role::find(id)->orders()->get()
     *
     * @return HasManyThrough
     */
    public function orders()
    {
        return $this->hasManyThrough(Order::class, Event::class);
    }
}
