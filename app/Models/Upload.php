<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $fillable = ['page_id', 'filename', 'hash', 'mime_type', 'is_image'];
    protected $table = 'files';
}
