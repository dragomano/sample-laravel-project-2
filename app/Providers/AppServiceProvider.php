<?php

namespace App\Providers;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Event;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Передаем переменную events в каждый шаблон при подключении layouts.nav
        View::composer('layouts.nav', function($view) {
            $events = Event::orderBy('title', 'asc')->get();

            $view->with('events', $events);
        });

        // Передаем переменную copyright в каждый шаблон при подключении layouts.footer
        View::composer('layouts.footer', function($view) {
            $view->with('copyright', '(C) 2019, Bugo');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Локализуем данные пользователей, генерируемые с помощью фабрики database/factories/OrderFactory
        $this->app->singleton(Generator::class, function () {
            return Factory::create('ru_RU');
        });
    }
}
