<?php

use App\Models\Order;
use Faker\Generator as Faker;

/**
 * Генерация случайной заявки с помощью библиотеки Faker
 */

$factory->define(Order::class, function (Faker $faker) {
    $randomEventNumber = $faker->numberBetween(1, 2);
    $gender = $faker->randomElement(['Male', 'Female']);

    $data = [
        'event_id'   => $randomEventNumber,
        'name'       => $gender === 'Male' ? $faker->firstNameMale : $faker->firstNameFemale,
        'surname'    => $gender === 'Male' ? $faker->lastName : $faker->lastName . 'а',
        'phone'      => $faker->phoneNumber,
        'email'      => $faker->email,
        'grade'      => $faker->randomElement(__('messages.grade_set')),
        'ip'         => $faker->ipv4,
        'utm'        => '/event/' . $randomEventNumber,
        'created_at' => $faker->dateTimeBetween('-3 months', '-2 days')
    ];

    return $data;
});
