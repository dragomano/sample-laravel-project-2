<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'role_id'     => 3,
                'title'       => 'Event A',
                'description' => 'Мероприятие А',
                'created_at'  => Carbon::now()
            ],
            [
                'role_id'     => 4,
                'title'       => 'Event B',
                'description' => 'Мероприятие B',
                'created_at'  => Carbon::now()
            ]
        ];

        DB::table('events')->insert($data);
    }
}
