<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::create([
            'title'       => 'Мероприятия',
            'alias'       => '/',
            'description' => 'Пример сложного сайта на Laravel',
            'body'        => 'Выберите в меню интересующее Вас мероприятие и оставьте заявку на запись.'
        ]);
    }
}