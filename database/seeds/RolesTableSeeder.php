<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name'        => 'admin',
            'description' => 'Администратор'
        ]);

        Role::create([
            'name'        => 'user',
            'description' => 'Пользователь'
        ]);

        Role::create([
            'name'        => 'maker_a',
            'description' => 'Представитель мероприятия A'
        ]);

        Role::create([
            'name'        => 'maker_b',
            'description' => 'Представитель мероприятия B'
        ]);
    }
}
