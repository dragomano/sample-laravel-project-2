<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'       => 'Demo User',
                'role_id'    => 2,
                'email'      => 'user@test.loc',
                'password'   => bcrypt('123'),
                'created_at' => Carbon::now()
            ],
            [
                'name'       => 'Event Maker A',
                'role_id'    => 3,
                'email'      => 'event_a@test.loc',
                'password'   => bcrypt('123'),
                'created_at' => Carbon::now()
            ],
            [
                'name'       => 'Event Maker B',
                'role_id'    => 4,
                'email'      => 'event_b@test.loc',
                'password'   => bcrypt('123'),
                'created_at' => Carbon::now()
            ],
            [
                'name'       => 'Super Admin',
                'role_id'    => 1,
                'email'      => 'admin@test.loc',
                'password'   => bcrypt('123'),
                'created_at' => Carbon::now()
            ]
        ];

        DB::table('users')->insert($data);
    }
}
