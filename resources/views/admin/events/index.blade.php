@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <h1>{{ __('messages.events') }}</h1>
        @if (!empty($events))
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('messages.responsible') }}</th>
                        <th>{{ __('messages.title') }}</th>
                        <th>{{ __('messages.desc') }}</th>
                        <th>{{ __('messages.created') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($events as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->role->description }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->description }}</td>
                            <td>{{ $item->created_at }}</td>
                        </tr>
                @endforeach
                @endif
            </div>
@endsection