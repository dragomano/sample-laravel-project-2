@extends('admin.layouts.app')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="jumbotron mt-3">
            <h1>{{ __('messages.admin_welcome_msg') }}</h1>
            <p class="lead">{{ __('messages.admin_welcome_desc') }}</p>
        </div>
    </div>
@endsection