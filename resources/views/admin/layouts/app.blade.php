
<!doctype html>
<html lang="en">
@include('admin.layouts.header')
<body>
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">{{ __('messages.admin') }}</a>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="/">{{ __('messages.site') }}</a>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        @include('admin.layouts.nav')

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            @yield('content')
        </main>
    </div>
</div>

@include('admin.layouts.footer')
</body>
</html>