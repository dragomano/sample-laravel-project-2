<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link{{ Request::path() === 'admin' ? ' active' : '' }}" href="{{ route('admin') }}">
                    <span data-feather="home"></span>
                    {{ __('messages.dashboard') }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link{{ Request::path() === 'admin/orders' ? ' active' : '' }}" href="{{ route('admin.orders') }}">
                    <span data-feather="file"></span>
                    {{ __('messages.orders') }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link{{ Request::path() === 'admin/events' ? ' active' : '' }}" href="{{ route('admin.events') }}">
                    <span data-feather="smile"></span>
                    {{ __('messages.events') }}
                </a>
            </li>
            @if (Auth::user()->role->name === 'admin')
            <li class="nav-item">
                <a class="nav-link{{ Request::path() === 'admin/pages' ? ' active' : '' }}" href="{{ route('admin.pages.index') }}">
                    <span data-feather="layers"></span>
                    {{ __('messages.pages') }}
                </a>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link{{ Request::path() === 'admin/users' ? ' active' : '' }}" href="{{ route('admin.users') }}">
                    <span data-feather="users"></span>
                    {{ __('messages.users') }}
                </a>
            </li>
            @if (Auth::user()->role->name === 'admin')
            <li class="nav-item">
                <a class="nav-link{{ Request::path() === 'admin/roles' ? ' active' : '' }}" href="{{ route('admin.roles') }}">
                    <span data-feather="settings"></span>
                    {{ __('messages.roles') }}
                </a>
            </li>
            @endif
        </ul>
    </div>
</nav>