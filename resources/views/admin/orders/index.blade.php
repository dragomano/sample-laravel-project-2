@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <h1>{{ __('messages.orders') }}</h1>
        @foreach ($orders as $item)
        <div class="row">
            <div class="col">
                <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                        <strong class="d-inline-block mb-2 text-success">{{ $item->event->title }}</strong>
                        <h3 class="mb-0">
                            <span class="text-dark">{{ $item->name }} {{ $item->surname }}</span>
                            @if (Auth::user()->role->name === 'admin')
                            <span class="order_del">
                                <a class="close" href="{{ route('admin.orders.delete', $item->id) }}?{{ time() }}" title="Удалить">
                                    <span aria-hidden="true">&times;</span>
                                </a>
                            </span>
                            @endif
                        </h3>
                        <div class="mb-1 text-muted">{{ $item->created_at }}</div>
                        <div class="mb-1 text-muted">{{ __('messages.phone') }}: {{ $item->phone }} | {{ __('messages.email') }}: <a href="mailto:{{ $item->email }}">{{ $item->email }}</a> | {{ __('messages.grade') }}: {{ $item->grade }}</div>
                        @if (Auth::user()->role->name === 'admin')
                        <div class="text-muted">
                            {{ __('messages.ip') }}: {{ $item->ip }}<br>{{ __('messages.utm') }}: {{ $item->utm }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="row justify-content-center">
            {{ $orders->links() }}
        </div>
    </div>
@endsection