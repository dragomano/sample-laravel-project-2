@extends('layouts.app')

@section('content')
    @php
        /** @var \Form */
    @endphp
    <div class="container">
        @include('layouts.errors')
        <div class="row justify-content-center">
            <div class="col-md-8 mb-1">
                {{ Form::model($page, ['route' => ['admin.pages.update', $page->id], 'files' => true]) }}
                <div class="card">
                    @method('PATCH')
                    <div class="card-header">
                        <div class="form-group">
                        {{ Form::label('title', __('messages.title')) }}
                        {{ Form::text('title', $page->title, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('alias', __('messages.alias')) }}
                            {{ Form::text('alias', $page->alias, ['class' => 'form-control', 'disabled' => true]) }}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {{ Form::label('description', __('messages.desc')) }}
                            {{ Form::text('description', $page->description, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                        {{ Form::label('body', __('messages.body')) }}
                        {{ Form::textarea('body', $page->body, ['class' => 'form-control', 'rows' => 4]) }}
                        </div>
                        @if ($page->file && $page->file->is_image)
                        <a href="/uploads/{{ $page->file->hash }}" data-fancybox data-caption="{{ $page->file->filename }}">
                            <img class="bd-placeholder-img card-img-top"
                                 src="/uploads/{{ $page->file->hash }}"
                                 alt="{{ $page->file->filename }}"
                                 style="width: 100px">
                        </a>
                        @endif
                        <div class="form-group">
                            {{ Form::label('image', __('messages.other_file')) }}
                            {{ Form::file('image', ['class' => 'form-control-file']) }}
                        </div>
                    </div>
                    <div class="card-footer text-muted">
                        {{ __('messages.created') }}{{ $page->created_at }}
                        @if ($page->updated_at > $page->created_at)
                            <span class="float-right">
                            {{ __('messages.updated') }}{{ $page->updated_at }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="text-center mt-2">
                    {{ Form::submit(__('messages.save'), ['class' => 'btn btn-primary']) }}
                    <a class="btn btn-secondary" href="{{ route('admin.pages.index') }}" role="button">
                        {{ __('messages.cancel') }}
                    </a>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection