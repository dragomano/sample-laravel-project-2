@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <h1>{{ __('messages.pages') }}</h1>
        @if (!empty($pages))
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('messages.title') }}</th>
                        <th>{{ __('messages.alias') }}</th>
                        <th>{{ __('messages.create_date') }}</th>
                        <th class="text-center">{{ __('messages.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($pages as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->alias }}</td>
                        <td>{{ $item->created_at }}</td>
                        <td class="text-center">
                            <a href="{{ route('admin.pages.edit', ['id' => $item->id]) }}" title="{{ __('messages.edit') }}">
                                <span data-feather="edit"></span>
                            </a>
                            <a href="{{ url($item->alias) }}" title="{{ __('messages.view') }}" target="_blank">
                                <span data-feather="eye"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
        @endif
    </div>
@endsection