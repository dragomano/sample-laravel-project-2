@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <h1>{{ __('messages.roles') }}</h1>
        @if (!empty($roles))
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('messages.designation') }}</th>
                        <th>{{ __('messages.desc') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($roles as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->description }}</td>
                        </tr>
                @endforeach
                @endif
            </div>
@endsection