@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <h1>{{ __('messages.users') }}</h1>
        @if (!empty($users))
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('messages.role') }}</th>
                        <th>{{ __('messages.name') }}</th>
                        <th>{{ __('messages.email') }}</th>
                        <th>{{ __('messages_create_date') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->role->description }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->created_at }}</td>
                        </tr>
                @endforeach
                @endif
            </div>
@endsection