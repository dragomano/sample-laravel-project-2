<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Письмо</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        body {
            font: 14px/1.4 Georgia, Serif;
        }
        #page-wrap {
            margin: 50px;
        }
        p {
            margin: 20px 0;
        }

        /*
        Generic Styling, for Desktops/Laptops
        */
        table {
            width: 100%;
            border-collapse: collapse;
        }
        /* Zebra striping */
        tr:nth-of-type(odd) {
            background: #eee;
        }
        th {
            background: #333;
            color: white;
            font-weight: bold;
        }
        td, th {
            padding: 6px;
            border: 1px solid #ccc;
            text-align: left;
        }
    </style>
</head>
<body>
    <h1>У вас новая заявка!</h1>
    <table>
        <tr>
            <th>Мероприятие</th>
            <th>Имя и фамилия</th>
            <th>{{ __('messages.phone') }}</th>
            <th>{{ __('messages.email') }}</th>
            <th>{{ __('messages.grade') }}</th>
        </tr>
        <tr>
            <th>{{ $data['event_title'] }}</th>
            <th>{{ $data['name'] . ' ' . $data['surname'] }}</th>
            <th>{{ $data['phone'] }}</th>
            <th>{{ $data['email'] }}</th>
            <th>{{ $data['grade'] }}</th>
        </tr>
    </table>
</body>
</html>