<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Письмо</title>
</head>
<body>
    <h1>Привет, {{ $data['name'] }}!</h1>
    <p>Спасибо, что зарегистрировались на наше мероприятие. Ваша заявка обрабатывается.</p>
</body>
</html>