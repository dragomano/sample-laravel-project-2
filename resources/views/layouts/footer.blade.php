    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/jquery.fancybox.min.js') }}" defer></script>
    <script src="//cdn.jsdelivr.net/npm/jquery@3/dist/jquery.min.js"></script>
    <script src="//stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

    @if (Request::is('event/*'))
    <script src="//unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
        feather.replace();
        jQuery(document).ready(function($) {
            $('form#order_form').on('submit', (function(e) {
                $.ajax({
                    type: $(this).attr('method'),
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    success: function() {
                        jQuery('#order_form')[0].reset();
                        jQuery('#results').html('<div class="alert alert-success fade show" role="alert">{{ __('messages.success_msg') }}</div>');
                    },
                    error: function() {
                        jQuery('#results').html('<div class="alert alert-danger fade show" role="alert">{{ __('messages.error_msg') }}</div>');
                    }
                });
                e.preventDefault();
            }));
        });
    </script>
    @endif

    <!-- {{ $copyright }} -->
