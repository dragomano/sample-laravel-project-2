@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="display-3">{{ $event->title }}</h1>

    @if (session('status'))
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-8 order-md-1">
            <h4 class="mb-3">{{ $event->description }}</h4>

            <div id="results" class="mt-2"></div>

            {{ Form::open(['route' => 'order.store', 'id' => 'order_form']) }}
                <div class="row">
                    <div class="col-md-6 mb-3">
                        {{ Form::hidden('event_id', $event->id) }}
                        {{ Form::label('name', __('messages.name')) }}
                        {{ Form::text('name', null, ['class' => 'form-control', 'required' => true]) }}
                    </div>
                    <div class="col-md-6 mb-3">
                        {{ Form::label('surname', __('messages.surname')) }}
                        {{ Form::text('surname', null, ['class' => 'form-control', 'required' => true]) }}
                    </div>
                </div>
                <div class="mb-3">
                    {{ Form::label('phone', __('messages.phone')) }}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><span data-feather="phone"></span></span>
                        </div>
                        {{ Form::text('phone', null, ['class' => 'form-control', 'required' => true]) }}
                    </div>
                </div>
                <div class="mb-3">
                    {{ Form::label('email', __('messages.email')) }}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><span data-feather="mail"></span></span>
                        </div>
                        {{ Form::text('email', null, ['class' => 'form-control', 'required' => true]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 mb-3">
                        {{ Form::label('grade', __('messages.grade')) }}
                        {{ Form::select('grade', __('messages.grade_set'), null, [
                            'class' => 'custom-select d-block w-100',
                            'required' => true
                         ]) }}
                    </div>
                </div>
                <div class="text-center mt-2">
                    {{ Form::submit(__('messages.send'), ['class' => 'btn btn-primary btn-lg btn-block']) }}
                </div>
            {{ Form::close() }}
        </div>

        <div class="col-md-4">
            <a href="https://lorempixel.com/1024/512/people" data-fancybox>
                <img class="bd-placeholder-img card-img-right"
                     src="https://lorempixel.com/200/248/people"
                     alt="Image"
                     style="width: 200px; height: 248px">
            </a>
        </div>
    </div>
@endsection
