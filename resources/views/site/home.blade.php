@extends('layouts.app')

@section('content')

@if (session('status'))
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="container">
    <h1 class="display-3">{{ $page->title }}</h1>
    @if ($page->file && $page->file->is_image)
    <div>
        <a href="/uploads/{{ $page->file->hash }}" data-fancybox data-caption="{{ $page->file->filename }}">
            <img class="bd-placeholder-img card-img-top"
                 src="/uploads/{{ $page->file->hash }}"
                 alt="{{ $page->file->filename }}"
                 style="width: 300px">
        </a>
    </div>
    @endif
    <p class="mt-4">{{ $page->body }}</p>
</div>

@endsection
