<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::resource('event', 'EventController')->only('show');
Route::resource('order', 'OrderController')->only('store');

Auth::routes();

Route::group(['namespace' => 'Admin', 'middleware' => 'auth'], function () {
    Route::get('admin', 'DashboardController@index')->name('admin');
    Route::resource('admin/pages', 'PageController')->only('index', 'edit', 'update')->names('admin.pages');
    Route::get('admin/orders', 'OrderController@index')->name('admin.orders');
    Route::get('admin/orders/{id}/delete', 'OrderController@destroy')->name('admin.orders.delete');
    Route::get('admin/events', 'EventController@index')->name('admin.events');
    Route::get('admin/users', 'UserController@index')->name('admin.users');
    Route::get('admin/roles', 'RoleController@index')->name('admin.roles');
});