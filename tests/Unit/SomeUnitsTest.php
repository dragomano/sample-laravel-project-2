<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\{Event, Order, Page, Role, User};

class SomeUnitsTest extends TestCase
{
    /**
     * Проверяем доступность главной страницы
     *
     * @return void
     */
    public function testHomeAreaStatus()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /**
     * Проверяем недоступность админки
     *
     * @return void
     */
    public function testNewsAreaStatus()
    {
        $response = $this->call('GET', 'admin');
        $response->assertStatus(302);
    }

    /**
     * Проверяем наличие модели Event для работы с мероприятиями
     *
     * @return void
     */
    public function testEventClassExist()
    {
        $event = new Event;
        $this->assertTrue($event instanceof Event);
    }

    /**
     * Проверяем наличие модели Order для работы с заявками
     *
     * @return void
     */
    public function testOrderClassExist()
    {
        $order = new Order;
        $this->assertTrue($order instanceof Order);
    }

    /**
     * Проверяем наличие модели Page для работы со страницами
     *
     * @return void
     */
    public function testPageClassExist()
    {
        $page = new Page;
        $this->assertTrue($page instanceof Page);
    }

    /**
     * Проверяем наличие модели Role для работы с ролями
     *
     * @return void
     */
    public function testRoleClassExist()
    {
        $role = new Role;
        $this->assertTrue($role instanceof Role);
    }

    /**
     * Проверяем наличие модели User для работы с пользователями
     *
     * @return void
     */
    public function testUserClassExist()
    {
        $user = new User;
        $this->assertTrue($user instanceof User);
    }
}